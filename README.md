.. SPDX-License-Identifier: GPL-2.0+

GitLab CI / Cross compilation runner container
===================================

Modified U-boot runner based on alpine linux for generic cross compilation

## Usage

```
docker build -t alpine:cross-compiler .
```
