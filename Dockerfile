# SPDX-License-Identifier: GPL-2.0+
# This Dockerfile is used to build an image containing basic stuff to be used
# to build U-Boot and run our test suites, by Tom Rini <trini@konsulko.com>
# Modified to run using alpine linux by Edward Halls <ehalls@gmail.com>

FROM alpine:edge
MAINTAINER Edward Halls <ehalls@gmail.com>
LABEL Description=" This image is for building U-Boot inside a container"

# Add LLVM
RUN apk update && apk add -y gnupg wget xz xz-libs xz-dev llvm10 llvm10-static llvm10-dev llvm10-test-utils

# Add build base
RUN apk add -y \
	automake \
	autopoint \
	bc \
	bison \
	boost1.75 \
	boost-dev \
	build-base \
	clang \
	coreutils \
	cpio \
	cppcheck \
	curl \
	dtc \
	dosfstools \
	dpkg \
	e2fsprogs \
	efitools \
	fakeroot \
	flex \
	sgdisk \
	git \
	graphviz \
	groff \
	grub-efi \
	iasl \
	imagemagick \
	iputils \
	libguestfs \
	libguestfs-dev \
	libgcrypt \
	libgcrypt-dev \
	libtool \
	isl \
	lz4 \
	lz4-static \
	pixman-static \
	sdl-dev \
	sdl2-dev \
	libressl-dev \
	eudev-dev \
	libusb-dev \
	lzop \
	libmount \
	mpfr4 \
	mpfr4-dev \
	mtd-utils \
	mtools \
	libressl \
	picocom \
	parted \
	python3 \
	python3-dev \
	py3-pip \
	py3-virtualenv \
	py3-sphinx \
	rpm2cpio \
	sbsigntool \
	sloccount \
	sparse \
	spinx \
	sudo \
	swig \
	util-linux \
	zip 

# Manually install the kernel.org "Crosstool" based toolchains for gcc-7.3
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-arc-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-aarch64-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-arm-linux-gnueabi.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-i386-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-m68k-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-mips-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-microblaze-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-nios2-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-powerpc-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-riscv32-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-riscv64-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-sh2-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-sparc64-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-nds32le-linux.tar.xz | tar -C /opt -xJ
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/10.1.0/x86_64-gcc-10.1.0-nolibc-xtensa-linux.tar.xz | tar -C /opt -xJ


# Manually install srecord
RUN wget -O - http://srecord.sourceforge.net/srecord-1.64.tar.gz | tar -C /tmp/srecord -xJ && \
	cd /tmp/srecord && \
	./configure && \
	make && make install \
	rm -rf /tmp/srecord

# Build GRUB UEFI targets for ARM & RISC-V, 32-bit and 64-bit
RUN git clone git://git.savannah.gnu.org/grub.git /tmp/grub && \
	cd /tmp/grub && \
	git checkout grub-2.04 && \
	./bootstrap && \
	mkdir -p /opt/grub && \
	./configure --target=aarch64 --with-platform=efi \
	CC=gcc \
	TARGET_CC=/opt/gcc-10.1.0-nolibc/aarch64-linux/bin/aarch64-linux-gcc \
	TARGET_OBJCOPY=/opt/gcc-10.1.0-nolibc/aarch64-linux/bin/aarch64-linux-objcopy \
	TARGET_STRIP=/opt/gcc-10.1.0-nolibc/aarch64-linux/bin/aarch64-linux-strip \
	TARGET_NM=/opt/gcc-10.1.0-nolibc/aarch64-linux/bin/aarch64-linux-nm \
	TARGET_RANLIB=/opt/gcc-10.1.0-nolibc/aarch64-linux/bin/aarch64-linux-ranlib && \
	make && \
	./grub-mkimage -O arm64-efi -o /opt/grub/grubaa64.efi --prefix= -d \
	grub-core cat chain configfile echo efinet ext2 fat halt help linux \
	lsefisystab loadenv lvm minicmd normal part_msdos part_gpt reboot \
	search search_fs_file search_fs_uuid search_label serial sleep test \
	true && \
	make clean && \
	./configure --target=arm --with-platform=efi \
	CC=gcc \
	TARGET_CC=/opt/gcc-10.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-gcc \
	TARGET_OBJCOPY=/opt/gcc-10.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-objcopy \
	TARGET_STRIP=/opt/gcc-10.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-strip \
	TARGET_NM=/opt/gcc-10.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-nm \
	TARGET_RANLIB=/opt/gcc-10.1.0-nolibc/arm-linux-gnueabi/bin/arm-linux-gnueabi-ranlib && \
	make && \
	./grub-mkimage -O arm-efi -o /opt/grub/grubarm.efi --prefix= -d \
	grub-core cat chain configfile echo efinet ext2 fat halt help linux \
	lsefisystab loadenv lvm minicmd normal part_msdos part_gpt reboot \
	search search_fs_file search_fs_uuid search_label serial sleep test \
	true && \
	make clean && \
	./configure --target=riscv64 --with-platform=efi \
	CC=gcc \
	TARGET_CC=/opt/gcc-10.1.0-nolibc/riscv64-linux/bin/riscv64-linux-gcc \
	TARGET_OBJCOPY=/opt/gcc-10.1.0-nolibc/riscv64-linux/bin/riscv64-linux-objcopy \
	TARGET_STRIP=/opt/gcc-10.1.0-nolibc/riscv64-linux/bin/riscv64-linux-strip \
	TARGET_NM=/opt/gcc-10.1.0-nolibc/riscv64-linux/bin/riscv64-linux-nm \
	TARGET_RANLIB=/opt/gcc-10.1.0-nolibc/riscv64-linux/bin/riscv64-linux-ranlib && \
	make && \
	./grub-mkimage -O riscv64-efi -o /opt/grub/grubriscv64.efi --prefix= -d \
	grub-core cat chain configfile echo efinet ext2 fat halt help linux \
	lsefisystab loadenv lvm minicmd normal part_msdos part_gpt reboot \
	search search_fs_file search_fs_uuid search_label serial sleep test \
	true && \
	make clean && \
	./configure --target=riscv32 --with-platform=efi \
	CC=gcc \
	TARGET_CC=/opt/gcc-10.1.0-nolibc/riscv32-linux/bin/riscv32-linux-gcc \
	TARGET_OBJCOPY=/opt/gcc-10.1.0-nolibc/riscv32-linux/bin/riscv32-linux-objcopy \
	TARGET_STRIP=/opt/gcc-10.1.0-nolibc/riscv32-linux/bin/riscv32-linux-strip \
	TARGET_NM=/opt/gcc-10.1.0-nolibc/riscv32-linux/bin/riscv32-linux-nm \
	TARGET_RANLIB=/opt/gcc-10.1.0-nolibc/riscv32-linux/bin/riscv32-linux-ranlib && \
	make && \
	./grub-mkimage -O riscv32-efi -o /opt/grub/grubriscv32.efi --prefix= -d \
	grub-core cat chain configfile echo efinet ext2 fat halt help linux \
	lsefisystab loadenv lvm minicmd normal part_msdos part_gpt reboot \
	search search_fs_file search_fs_uuid search_label serial sleep test \
	true && \
	rm -rf /tmp/grub

RUN git clone git://git.qemu.org/qemu.git /tmp/qemu && \
	cd /tmp/qemu && \
	git submodule update --init dtc && \
	git checkout v4.2.0 && \
	./configure --prefix=/opt/qemu --target-list="aarch64-softmmu,arm-softmmu,i386-softmmu,mips-softmmu,mips64-softmmu,mips64el-softmmu,mipsel-softmmu,ppc-softmmu,riscv32-softmmu,riscv64-softmmu,x86_64-softmmu,xtensa-softmmu" && \
	make -j$(nproc) all install && \
	rm -rf /tmp/qemu

# Create our user/group
RUN echo builder ALL=NOPASSWD: ALL > /etc/sudoers.d/builder
RUN useradd -m -U builder
USER builder:builder

# Create the buildman config file
RUN /bin/echo -e "[toolchain]\nroot = /usr" > ~/.buildman
RUN /bin/echo -e "kernelorg = /opt/gcc-10.1.0-nolibc/*" >> ~/.buildman
RUN /bin/echo -e "\n[toolchain-alias]\nsh = sh2" >> ~/.buildman
RUN /bin/echo -e "\nnds32 = nds32le" >> ~/.buildman;
RUN /bin/echo -e "\nriscv = riscv64" >> ~/.buildman
RUN /bin/echo -e "\nsandbox = x86_64" >> ~/.buildman
RUN /bin/echo -e "\nx86 = i386" >> ~/.buildman;
